# datapine frontend test #

Once you have cloned this repo, then please follow the instructions below to continue with the test. If you have any problems or need any help, then please let us know.

### This is the current datapine frontend test for those wishing to be part of the datapine team ###

### How do I get set up? ###

* Clone this repo
* Install yeoman or simply just bower and grunt
* Once they have been installed go to the directory and install the grunt-cli -> 'npm install -g grunt-cli' and then 'npm install'
* Next run 'bower install' to install all the dependencies required
* Once everything has been installed simply run 'grunt server' and begin your test
* There is a testing section in the repo, but we do not expect you to write any tests for this particular test

### Who do I talk to? ###

Once finished ideally you would push to your own repo, send us the link with any comments you would like to make and then wait a few days for our response (depends heavily upon how busy we currently are :))