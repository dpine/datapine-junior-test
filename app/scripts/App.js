/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager'
],
function (
    Backbone, LayoutManager
) {
    return {
        // Do whatever we need to kickstart the app
        initialize: function (Router) {
            this.router = new Router();
            Backbone.Layout.configure({
                fetchTemplate: function(html) {
                    // Check for a global JST object.  When you build your templates for
                    // production, ensure they are all attached here.
                    var JST = window.JST || {};

                    // If the path exists in the object, use it instead of fetching remotely.
                    if (JST[html]) {
                      return JST[html];
                    }

                    return JST[html] = _.template(html);
                },
                manage: true
            });
            Backbone.history.start();
            this.router.init();
        }
    };
});