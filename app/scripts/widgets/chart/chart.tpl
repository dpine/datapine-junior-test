<div class="chart"></div>
<div class="chart-options">
    <div class="chart-option chart-type chart-type-line" data-type="line">Line</div>
    <div class="chart-option chart-type chart-type-column" data-type="column">Column</div>
    <div class="chart-option chart-type chart-type-pie" data-type="pie">Pie</div>
    <div class="chart-option expand-chart">Expand Chart</div>
</div>