/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    '../chart/chart',
    'text!./index.tpl'
],
function (Backbone, LayoutManager, ChartView, indexTemplate) {
    return Backbone.Layout.extend({
        template: indexTemplate,
        initialize: function () {
        },
        beforeRender: function () {
            /* This will load all charts from a json source app/data/chart{i}.json */
            for (var i=0; i<4; i++) {
                this.insertView('.chart-list', new ChartView({ chartId: (i + 1), canExpand: true }));
            }
        },
        afterRender: function () {
        }
    });
});