<h1>Welcome to the datapine junior test</h1>
<h2>Your task</h2>
<p>As you can see this test looks rather awful. We would like you to use your CSS &amp; HTML skills to improve both the look and feel of this page.</p>
<p>Also there are a few problems, which we would like you to fix. Two of which are mentioned below, extra points if you can find more...</p>
<ul>
    <li>The <span class="important-text">Expand Chart</span> button currently does nothing at all. Could you please have it expand into something like a lightbox</li>
    <li>If you look at <span class="important-text">Chart 4</span> you will notice that it can be switched to a pie chart. However, the datalabels for the Pie Chart slices does not display the correct label (Point 1, Point 2 &amp;c.)</li>
</ul>
<div class="chart-list"></div>