<h1>Some basic information to get you started</h1>
<h2>Useful links</h2>
<ul>
    <li><a href="http://yeoman.io/">http://yeoman.io/</a></li>
    <li><a href="http://gruntjs.com/">http://gruntjs.com/</a></li>
    <li><a href=">http://underscorejs.org/">http://underscorejs.org/</a></li>
    <li><a href="http://backbonejs.org/">http://backbonejs.org/</a></li>
    <li><a href="https://github.com/tbranyen/backbone.layoutmanager">https://github.com/tbranyen/backbone.layoutmanager</a></li>
    <li><a href="http://http://api.highcharts.com/highcharts/">http://http://api.highcharts.com/highcharts/</a></li>
</ul>
<h2>Getting started</h2>
<p>
    The project is layed out to be a bit similar to our current app, in terms of code structure. You will find, inside <span class="important-text">app/scripts</span>, all the JS, LESS and template files used in the application. If you create a new widget/page, please try to follow the layout shown in the app. If you would like to add a CSS class to a widget itself please checkout the <span class="important-text">menu widget</span> to see how this is accomplished.
</p>
<p>
    Please try to keep the CSS code looking clean and where possible tidy up the existing code. The use of site-wide variables would be rather helpful in this regard. The initial LESS file being located in <span class="important-text">styles/main.less</span>
</p>
<p>
    Lastly, we don't expect your test to be perfect as you do not have all the time in the world to complete it, but we do expect the project to be working and looking better than it currently is. How much better is entirely your decision.
</p>